# Formulario

Este proyecto es un panel de consulta de información compuesto por tres componentes. 
El primero es un formulario de registro donde el usuario diligencia los datos, entre ellos contraseña y correo, los cuales son guardados en una base de datos. Despues de almacenados se direccionara al segundo componente de Inicio de sesión, aquí el usuario por medio de la contraseña y correo con los cuales se registró previamente, podrá ingresar al tercer componente del sistema sistema, donde podrá visualizar toda su información guardada en la base de datos, correspondiente a los datos con los cuales se registró en un principio.
Todos los campos de registro y de inicio de sesión tienen sus respectivas validaciones para que solo pueda ingresar los caracteres permitidos o para que no pueda guardar la información si todos los campos no se encuentran diligenciados.
Para el inicio de sesión se hace una validacíon correspondiente con la base de datos, si ingresa la contraseña o correo incorrectos, mostrará un aviso indicando cual es el error

Este proyecto fue generado con [Angular CLI] version 12.2.12.

## Estrategias

 Para la realización del proyecto me base en el curso y tutoriales de Angular, en investigaciones y consultas realizadas en la web y en algunos conocimientos que ya tenia en HTML, CSS y Bootstrap

## Dificultades

La mayor dificultad que presenté fue la falta de conocimiento en cuanto al framework en cuestión, primera vez que trabajaba con Angular pero despues de que se conoce y aprende la estructura es muy cómodo trabajarlo
