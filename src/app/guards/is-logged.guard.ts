import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';

@Injectable({
  providedIn: 'root'
})
export class IsLoggedGuard implements CanActivate {

  constructor(private loginService:LoginService, private router:Router) {}

  canActivate(): boolean {
    if (this.loginService.LoggedOn() )
    {
      return true;
    }
    else
    {
      this.router.navigate([ '/informacion', this.loginService.UsuarioId()] );
      return false;
    }
  }
  
}
