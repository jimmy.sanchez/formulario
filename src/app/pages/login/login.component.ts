import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  login1 : any;
  formulario!: FormGroup;

  constructor( private router:Router, private formBuilder: FormBuilder, private loginService: LoginService ) { 

    this.crearFormulario();

  }

  ngOnInit(): void {
  }

  get validacionEmail() {
    return this.formulario.get('email')?.invalid && this.formulario.get('email')?.touched
  }

  get validacionPassword() {
    return this.formulario.get('password')?.invalid && this.formulario.get('password')?.touched
  }

  crearFormulario() {
    this.formulario = this.formBuilder.group({
      email:    ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      password: ['', [Validators.required, Validators.minLength(8)]],
    });
  }

  login() {
    if  (this.formulario.invalid)
    {
      return Object.values(this.formulario.controls).forEach(control => (
        control.markAllAsTouched()
      ));
    }
    else
    {
      this.setLogin();
    }
  }

  setLogin() {
     this.login1 = { email: this.formulario.get('email')?.value,
                     password: this.formulario.get('password')?.value 
                   }

    this.loginService.guardar(this.login1).subscribe(data => {

      console.log(data);
      if (data) {
        
        this.router.navigate( ['/informacion', data.user.id] );
      }
    }, e => {
     if (e.error.code == 0){
      Swal.fire({
        icon: 'error',
        text: 'Contraseña Incorrecta'
        })
    }
    else if (e.error.code == 1){
         Swal.fire({
           icon: 'error',
           text: 'Correo Incorrecto'
           })
        }
        else
        {
          Swal.fire({
            icon: 'error',
            text: 'No ha sido posible realizar la autenticación'
            })
        }
    })
  }
}
