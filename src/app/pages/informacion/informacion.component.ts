import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { InformacionService } from 'src/app/services/informacion.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-informacion',
  templateUrl: './informacion.component.html',
  styleUrls: ['./informacion.component.css']
})
export class InformacionComponent implements OnInit {

  id: any;
  nombre!: string;
  identificacion!: string;
  celular!: string;
  email!: string;
  password!: string;
  departamento!: string;
  ciudad!: string;
  barrio!: string;
  direccion!: string;
  salario!: number;
  otrosIngresos!: number;
  mensuales!: string;
  financieros!: number;

  constructor( private activatedRoute: ActivatedRoute, private router: Router, private informacionService:InformacionService, private loginService:LoginService ) { }
 
  ngOnInit(): void 
  {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.id);
    
    this.informacionService.mostrar({id :this.id}).subscribe(data => {
      if (data) {
          this.nombre = data.user.nombre;
          this.identificacion = data.user.identificacion;
          this.celular = data.user.celular;
          this.email = data.user.email;
          this.departamento = data.user.departamento;
          this.ciudad = data.user.ciudad;
          this.barrio = data.user.barrio;
          this.direccion = data.user.direccion;
          this.salario = data.user.salario;
          this.otrosIngresos = data.user.otrosIngresos;
          this.mensuales = data.user.mensuales;
          this.financieros = data.user.financieros;

      } 
    }, e => {
       console.log('error front');
       
    })
  }  

  Salir(){
    this.loginService.logout();
    this.router.navigateByUrl('/login');
  }
}
