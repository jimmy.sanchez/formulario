import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']
})
export class ReactiveComponent implements OnInit {

  forma!: FormGroup; 
  usuario!: Usuario;

  constructor( private fb: FormBuilder, private usuarioService:UsuarioService, private router:Router ) { 
  
  this.crearFormulario();

  }

  ngOnInit(): void {
  }

  get nombreNoValido() {
    return this.forma.get('datosPersonales.nombre')?.invalid && this.forma.get('datosPersonales.nombre')?.touched
  }

  get identificacionNoValida() {
    return this.forma.get('datosPersonales.identificacion')?.invalid && this.forma.get('datosPersonales.identificacion')?.touched
  }

  get celularNoValido() {
    return this.forma.get('datosPersonales.celular')?.invalid && this.forma.get('datosPersonales.celular')?.touched
  }

  get correoNoValido() {
    return this.forma.get('datosPersonales.correo')?.invalid && this.forma.get('datosPersonales.correo')?.touched
  }

  get contrasenaNoValida() {
    return this.forma.get('datosPersonales.contrasena')?.invalid && this.forma.get('datosPersonales.contrasena')?.touched
  }

  get departamentoNoValido() {
    return this.forma.get('residencia.departamento')?.invalid && this.forma.get('residencia.departamento')?.touched
  }

  get ciudadNoValida() {
    return this.forma.get('residencia.ciudad')?.invalid && this.forma.get('residencia.ciudad')?.touched
  }

  get barrioNoValido() {
    return this.forma.get('residencia.barrio')?.invalid && this.forma.get('residencia.barrio')?.touched
  }

  get direccionNoValida() {
    return this.forma.get('residencia.direccion')?.invalid && this.forma.get('residencia.direccion')?.touched
  }

  get salarioNoValido() {
    return this.forma.get('datosFinancieros.salario')?.invalid && this.forma.get('datosFinancieros.salario')?.touched
  }

  get ingresosNoValido() {
    return this.forma.get('datosFinancieros.otrosIngresos')?.invalid && this.forma.get('datosFinancieros.otrosIngresos')?.touched
  }

  get mensualesNoValido() {
    return this.forma.get('datosFinancieros.gastosMensuales')?.invalid && this.forma.get('datosFinancieros.gastosMensuales')?.touched
  }

  get financierosNoValido() {
    return this.forma.get('datosFinancieros.gastosFinancieros')?.invalid && this.forma.get('datosFinancieros.gastosFinancieros')?.touched
  }

  crearFormulario() {
    this.forma = this.fb.group({
      datosPersonales: this.fb.group({ 
        nombre:         ['', [Validators.required]],
        identificacion: ['', [Validators.required, Validators.minLength(6), Validators.pattern('[0-9]*')]],
        celular:        ['', [Validators.required, Validators.minLength(10), Validators.pattern('[0-9]*')]],
        correo:         ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
        contrasena:     ['', [Validators.required, Validators.minLength(8)]],
      }),
      residencia: this.fb.group({
        departamento: ['', Validators.required ],
        ciudad:       ['', Validators.required ],
        barrio:       ['', Validators.required ],
        direccion:    ['', Validators.required ],
      }),
      datosFinancieros: this.fb.group({
        salario:           ['', [Validators.required, Validators.pattern('[0-9]*')]],
        otrosIngresos:     ['', [Validators.pattern('[0-9]*')]],
        gastosMensuales:   ['', [Validators.required, Validators.required, Validators.pattern('[0-9]*')]],
        gastosFinancieros: ['', [Validators.pattern('[0-9]*')]],
      }) 
    });
  }



  guardar() {
    if  (this.forma.invalid)
    {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Por favor digite todos los campos',
      })
      
      return Object.values(this.forma.controls).forEach(control => (
        control.markAllAsTouched()
      ));
    }
    else
    {
      this.setUsuario();
    }
    //this.forma.reset()
  }

  setUsuario() {
    this.usuario = {
      nombre: this.forma.get('datosPersonales.nombre')?.value,
      identificacion: this.forma.get('datosPersonales.identificacion')?.value,
      celular: this.forma.get('datosPersonales.celular')?.value,
      email: this.forma.get('datosPersonales.correo')?.value,
      password: this.forma.get('datosPersonales.contrasena')?.value,
      departamento: this.forma.get('residencia.departamento')?.value,
      ciudad: this.forma.get('residencia.ciudad')?.value,
      barrio: this.forma.get('residencia.barrio')?.value,
      direccion: this.forma.get('residencia.direccion')?.value,
      salario: this.forma.get('datosFinancieros.salario')?.value,
      otrosIngresos: this.forma.get('datosFinancieros.otrosIngresos')?.value,
      mensuales: this.forma.get('datosFinancieros.gastosMensuales')?.value,
      financieros: this.forma.get('datosFinancieros.gastosFinancieros')?.value,
    }

    this.usuarioService.guardar(this.usuario).subscribe(data => {
      if (data) {
        Swal.fire({
          //position: 'top-end',
          icon: 'success',
          title: 'Registro guardado con éxito',
          showConfirmButton: false,
          timer: 1500
        });
        this.router.navigate(['/login']);
      }
    }, e => {
      console.log(e);
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Ha Ocurrido un Error',
      });
    })
  }
}
