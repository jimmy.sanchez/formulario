export interface Usuario {

    nombre: string;
    identificacion: number;
    celular: number;
    email: string;
    password: string;
    departamento: string;
    ciudad: string;
    barrio: string;
    direccion: string;
    salario: number;
    otrosIngresos: number;
    mensuales: number;
    financieros: number;
}
