import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { Usuario } from '../models/usuario';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  apiURL = environment.apiURL;

  constructor( private http: HttpClient ) { }
  
  guardar(usuario:Usuario):Observable<any>{
  return this.http.post(`${this.apiURL}users/saveUsers`, usuario)
  }  
}
