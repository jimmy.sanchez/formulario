import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn() {
    throw new Error('Method not implemented.');
  }

  apiURL = environment.apiURL;

  userToken: string;
  userId: string;

  constructor( private http: HttpClient ) { 

  }

    guardar( login: any ):Observable<any>{
    return this.http.post(`${this.apiURL}login/saveLogin`, login
                         ).pipe(map( resp  => {
                          console.log('Entró en el mapa del RJXS');
                          this.guardarToken( resp['token'] );
                          this.guardarId( resp['userId'] )
                          console.log(resp ['userId'])
                          return resp;
                         }))
    }

    logout(){
      localStorage.removeItem('token');
      localStorage.removeItem('id');
    }

    private guardarToken( idToken: string ){

      this.userToken = idToken;
      localStorage.setItem('token', idToken);
    }

    leerToken() {
      if ( localStorage.getItem('token') ) 
      {
        this.userToken = localStorage.getItem('token');
      }
      else
      {
        this.userToken = '';
      }
      return this.userToken;
    }

    private guardarId( idUser: string ){

      this.userId = idUser;
      localStorage.setItem('id', idUser);
    }

    isLogged(){
      let result = false;

      if(localStorage.getItem('token'))
      {
        result = true;
      }
      return result;
    }

    LoggedOn(){
      let result = true;

      if(localStorage.getItem('token'))
      {
        result = false;
      }
      return result;
    }

    UsuarioId () {
      this.userId = localStorage.getItem('id');
      return parseInt (this.userId); 
    }
}
