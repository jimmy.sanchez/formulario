import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { IsLoggedGuard } from './guards/is-logged.guard';
import { InformacionComponent } from './pages/informacion/informacion.component';
import { LoginComponent } from './pages/login/login.component';
import { ReactiveComponent } from './pages/reactive/reactive.component';

const routes: Routes = [
  { path: 'reactivo', component: ReactiveComponent, canActivate: [ IsLoggedGuard ] },
  { path: 'login', component: LoginComponent, canActivate: [ IsLoggedGuard ] },
  { path: 'informacion/:id', component: InformacionComponent, canActivate: [ AuthGuard ] },
  { path: '**', pathMatch: 'full', redirectTo: 'login' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
